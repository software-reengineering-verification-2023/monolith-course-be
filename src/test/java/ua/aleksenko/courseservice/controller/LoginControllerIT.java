package ua.aleksenko.courseservice.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.transaction.annotation.Transactional;

import ua.aleksenko.courseservice.AbstractIntegrationTest;
import ua.aleksenko.courseservice.util.ApiUtil;

@AutoConfigureMockMvc
class LoginControllerIT extends AbstractIntegrationTest {

	@Autowired
	private ApiUtil apiUtil;

	@Test
	@Transactional
	void login() {
		// Given
		apiUtil.registerDefaultUser();
		// When
		// Then
		apiUtil.loginWithDefaultUser();
	}
}
