package ua.aleksenko.courseservice.controller;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ua.aleksenko.courseservice.util.TestDataConstant.TEST_USER_EMAIL;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.SneakyThrows;
import ua.aleksenko.courseservice.AbstractIntegrationTest;
import ua.aleksenko.courseservice.model.dto.CourseDetailsDto;
import ua.aleksenko.courseservice.model.dto.CourseItemDto;
import ua.aleksenko.courseservice.model.dto.CreateCourseDto;
import ua.aleksenko.courseservice.model.dto.PageResponse;
import ua.aleksenko.courseservice.model.dto.UpdateCourseDto;
import ua.aleksenko.courseservice.model.entity.Course;
import ua.aleksenko.courseservice.model.specification.FieldType;
import ua.aleksenko.courseservice.model.specification.FilterRequest;
import ua.aleksenko.courseservice.model.specification.Operator;
import ua.aleksenko.courseservice.model.specification.SearchRequest;
import ua.aleksenko.courseservice.repository.CourseRepository;
import ua.aleksenko.courseservice.util.ApiUtil;

@AutoConfigureMockMvc
class CourseControllerIT extends AbstractIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private CourseRepository courseRepository;

	@Autowired
	private ApiUtil apiUtil;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	@WithMockUser(authorities = "ADMIN", username = TEST_USER_EMAIL)
	@SneakyThrows
	void searchCourses() {
		// Given
		for (int i = 0; i < 5; i++) {
			apiUtil.createDefaultCourse();
		}
		SearchRequest searchRequest = new SearchRequest(
				List.of(new FilterRequest("title", Operator.LIKE, FieldType.STRING, "Java", null, null)),
				null, 1, 3
		);
		// When
		String content = mockMvc.perform(post("/api/v1/course/search")
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(searchRequest)))
				.andExpect(status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();
		PageResponse<CourseItemDto> page = objectMapper.readValue(content, new TypeReference<>() {
		});
		assertEquals(1, page.getPageNumber());
		assertEquals(3, page.getPageSize());
		assertEquals(2, page.getTotalPages());
		assertEquals(5, page.getTotalElements());
		List<CourseItemDto> courses = page.getContent();
		assertEquals(3, courses.size());
	}

	@Test
	@SneakyThrows
	@WithMockUser(authorities = "ADMIN", username = TEST_USER_EMAIL)
	void getCourseDetails() {
		// Given
		UUID id = apiUtil.createDefaultCourse();
		CreateCourseDto course = apiUtil.getDefaultCourse();
		// When
		String content = mockMvc.perform(get("/api/v1/course/" + id))
				.andExpect(status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();

		// Then
		CourseDetailsDto courseDetails = objectMapper.readValue(content, CourseDetailsDto.class);
		assertEquals(course.title(), courseDetails.title());
		assertEquals(course.description(), courseDetails.description());
		assertEquals(course.imageUrl(), courseDetails.imageUrl());
		assertEquals(course.category(), courseDetails.category());
		assertEquals(course.source(), courseDetails.source());
	}

	@Test
	@SneakyThrows
	@WithMockUser(authorities = "USER", username = TEST_USER_EMAIL)
	void getAllCategories() {
		// When
		// Then
		mockMvc.perform(get("/api/v1/course/categories"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.*", hasSize(9)))
				.andExpect(jsonPath("$.*", containsInAnyOrder(
						"Frontend", "Backend", "DevOps", "Marketing",
						"Graphic", "Video", "GameDev", "Testing", "Finance"
				)));
	}

	@Test
	@SneakyThrows
	@WithMockUser(authorities = "USER", username = TEST_USER_EMAIL)
	void getAllLanguages() {
		// When
		// Then
		mockMvc.perform(get("/api/v1/course/languages"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.*", hasSize(2)))
				.andExpect(jsonPath("$.*", containsInAnyOrder("English", "Ukrainian")));
	}

	@Test
	@SneakyThrows
	@WithMockUser(authorities = "ADMIN", username = TEST_USER_EMAIL)
	@Transactional
	void deleteCourse() {
		// Given
		UUID id = apiUtil.createDefaultCourse();
		// When
		mockMvc.perform(delete("/api/v1/course/delete/" + id))
				.andExpect(status().isNoContent());
		// Then
		Optional<Course> courseOptional = courseRepository.findById(id);
		assertTrue(courseOptional.isEmpty());
	}

	@Test
	@SneakyThrows
	@WithMockUser(authorities = "ADMIN", username = TEST_USER_EMAIL)
	@Transactional
	void createCourse() {
		// Given
		CreateCourseDto newCourse = apiUtil.getDefaultCourse();
		// When
		UUID id = apiUtil.createDefaultCourse();
		// Then
		Optional<Course> courseOptional = courseRepository.findById(id);
		assertTrue(courseOptional.isPresent());
		Course course = courseOptional.get();
		assertEquals(newCourse.title(), course.getTitle());
		assertEquals(newCourse.source(), course.getSource().getName());
		assertEquals(newCourse.description(), course.getDescription());
		assertEquals(newCourse.imageUrl(), course.getImageUrl());
		assertEquals(newCourse.language(), course.getLanguage().getName());
		assertEquals(newCourse.category(), course.getCategory().getName());
	}

	@Test
	@SneakyThrows
	@WithMockUser(authorities = "ADMIN", username = TEST_USER_EMAIL)
	@Transactional
	void updateCourse() {
		// Given
		UUID id = apiUtil.createDefaultCourse();
		UpdateCourseDto updateCourse = new UpdateCourseDto(id, "New title", "new desc",
				"Artemii Aleksenko", "DevOps", "Ukrainian", "new some url",
				List.of(), List.of(), List.of("url/lesson1.aws", "url/lesson2.aws"));
		// When
		mockMvc.perform(put("/api/v1/course/update")
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(updateCourse)))
				.andExpect(status().isOk());
		// Then
		Optional<Course> courseOptional = courseRepository.findById(id);
		assertTrue(courseOptional.isPresent());
		Course course = courseOptional.get();
		assertEquals(updateCourse.title(), course.getTitle());
		assertEquals(updateCourse.title(), course.getTitle());
		assertEquals(updateCourse.source(), course.getSource().getName());
		assertEquals(updateCourse.description(), course.getDescription());
		assertEquals(updateCourse.imageUrl(), course.getImageUrl());
		assertEquals(updateCourse.language(), course.getLanguage().getName());
		assertEquals(updateCourse.category(), course.getCategory().getName());
	}
}
