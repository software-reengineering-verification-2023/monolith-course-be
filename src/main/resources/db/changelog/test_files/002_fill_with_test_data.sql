-- liquibase formatted sql
-- changeset artemii.aleksenko:002_fill_with_test_data.sql
INSERT INTO source (id, name, description)
VALUES ('3a11ddb0-e420-11ed-b5ea-0242ac120002', 'Amazon', 'The best cloud provider');

INSERT INTO source (id, name, description)
VALUES ('2a11ddb0-e420-11ed-b5ea-0242ac120002', 'Google Academy', 'Some google desc');

INSERT INTO source (id, name, description)
VALUES ('1a11ddb0-e420-11ed-b5ea-0242ac120002', 'Tom Smith', 'The best cloud provider');

INSERT INTO source (id, name, description)
VALUES ('4a11ddb0-e420-11ed-b5ea-0242ac120002', 'Coursera', 'Coursera desc');


INSERT INTO course (id, title, description, image_url, language, category, total_duration, created_at, updated_at, source_id)
VALUES ('a12480a4-e420-11ed-b5ea-0242ac120002', 'Course 2', 'Best course ever', 'https://a0.awsstatic.com/libra-css/images/logos/aws_logo_smile_1200x630.png',
        'ENGLISH', 'BACKEND', INTERVAL '1 hour 50 minutes 10 seconds', '2023-04-24 14:00:18.000000', '2023-04-26 14:00:22.000000',
        '2a11ddb0-e420-11ed-b5ea-0242ac120002');

INSERT INTO course (id, title, description, image_url, language, category, total_duration, created_at, updated_at, source_id)
VALUES ('a22480a4-e420-11ed-b5ea-0242ac120002', 'Course 3', 'Best course ever', 'https://a0.awsstatic.com/libra-css/images/logos/aws_logo_smile_1200x630.png',
        'ENGLISH', 'BACKEND', INTERVAL '1 hour 50 minutes 10 seconds', '2023-04-24 14:00:18.000000', '2023-04-26 14:00:22.000000',
        '1a11ddb0-e420-11ed-b5ea-0242ac120002');

INSERT INTO course (id, title, description, image_url, language, category, total_duration, created_at, updated_at, source_id)
VALUES ('a32480a4-e420-11ed-b5ea-0242ac120002', 'Course 4', 'Best course ever', 'https://a0.awsstatic.com/libra-css/images/logos/aws_logo_smile_1200x630.png',
        'ENGLISH', 'BACKEND', INTERVAL '1 hour 50 minutes 10 seconds', '2023-04-24 14:00:18.000000', '2023-04-26 14:00:22.000000',
        '3a11ddb0-e420-11ed-b5ea-0242ac120002');

INSERT INTO course (id, title, description, image_url, language, category, total_duration, created_at, updated_at, source_id)
VALUES ('a42480a4-e420-11ed-b5ea-0242ac120002', 'Course 5', 'Best course ever', 'https://a0.awsstatic.com/libra-css/images/logos/aws_logo_smile_1200x630.png',
        'ENGLISH', 'BACKEND', INTERVAL '1 hour 50 minutes 10 seconds', '2023-04-24 14:00:18.000000', '2023-04-26 14:00:22.000000',
        '1a11ddb0-e420-11ed-b5ea-0242ac120002');

INSERT INTO course (id, title, description, image_url, language, category, total_duration, created_at, updated_at, source_id)
VALUES ('a52480a4-e420-11ed-b5ea-0242ac120002', 'Course 6', 'Best course ever', 'https://a0.awsstatic.com/libra-css/images/logos/aws_logo_smile_1200x630' ||
                                                                                '.png',
        'ENGLISH', 'BACKEND', INTERVAL '1 hour 50 minutes 10 seconds', '2023-04-24 14:00:18.000000', '2023-04-26 14:00:22.000000',
        '3a11ddb0-e420-11ed-b5ea-0242ac120002');

INSERT INTO course (id, title, description, image_url, language, category, total_duration, created_at, updated_at, source_id)
VALUES ('a62480a4-e420-11ed-b5ea-0242ac120002', 'Course 7', 'Best course ever', 'https://a0.awsstatic.com/libra-css/images/logos/aws_logo_smile_1200x630.png',
        'ENGLISH', 'BACKEND', INTERVAL '1 hour 50 minutes 10 seconds', '2023-04-24 14:00:18.000000', '2023-04-26 14:00:22.000000',
        '4a11ddb0-e420-11ed-b5ea-0242ac120002');

INSERT INTO lesson (id, title, video_url, duration, course_id)
VALUES ('b52480a4-e420-11ed-b5ea-0242ac120002', 'Lesson1', 'https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.jpg', '25m 12s',
        'a62480a4-e420-11ed-b5ea-0242ac120002'),
       ('c52480a4-e420-11ed-b5ea-0242ac120002', 'Lesson2', 'https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.jpg', '25m 12s',
        'a62480a4-e420-11ed-b5ea-0242ac120002'),
       ('d52480a4-e420-11ed-b5ea-0242ac120002', 'Lesson3', 'https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.jpg', '25m 12s',
        'a62480a4-e420-11ed-b5ea-0242ac120002');


INSERT INTO library.book(id, title, author, description, image_url, source_url, language, category, released_at)
VALUES ('b52480a4-e420-11ed-b5ea-0242ac120002', 'Refactoring', 'Marting Fawler', 'desc',
        'https://a0.awsstatic.com/libra-css/images/logos/aws_logo_smile_1200x630.png',
        'https://vss6.coursehunter.net/ts-awsbook/osnovy-aws-aws-dlya-realnogo-mira_1.pdf', 'ENGLISH', 'BACKEND', now())
