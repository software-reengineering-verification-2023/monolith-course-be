package ua.aleksenko.courseservice.service;

import ua.aleksenko.courseservice.model.entity.User;

public interface UserService {

	User findUser(String email);
}
