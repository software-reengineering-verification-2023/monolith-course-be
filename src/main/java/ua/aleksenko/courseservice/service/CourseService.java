package ua.aleksenko.courseservice.service;

import java.util.List;
import java.util.UUID;

import ua.aleksenko.courseservice.model.dto.CourseDetailsDto;
import ua.aleksenko.courseservice.model.dto.CourseItemDto;
import ua.aleksenko.courseservice.model.dto.CreateCourseDto;
import ua.aleksenko.courseservice.model.dto.PageResponse;
import ua.aleksenko.courseservice.model.dto.UpdateCourseDto;
import ua.aleksenko.courseservice.model.specification.SearchRequest;

public interface CourseService {

	PageResponse<CourseItemDto> searchCourses(SearchRequest request);

	CourseDetailsDto getCourseDetails(UUID id);

	List<String> getAllCategories();

	List<String> getAllLanguages();

	void deleteCourse(UUID id);

	UUID create(CreateCourseDto dto);

	void update(UpdateCourseDto dto);
}
