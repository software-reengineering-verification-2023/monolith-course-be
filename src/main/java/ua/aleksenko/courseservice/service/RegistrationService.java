package ua.aleksenko.courseservice.service;

import ua.aleksenko.courseservice.model.dto.AuthenticationResponseDto;
import ua.aleksenko.courseservice.model.dto.RegistrationRequestDto;

public interface RegistrationService {

	AuthenticationResponseDto registerUser(RegistrationRequestDto dto);
}
