package ua.aleksenko.courseservice.service.impl;

import java.util.function.Consumer;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ua.aleksenko.courseservice.model.dto.AuthenticationResponseDto;
import ua.aleksenko.courseservice.model.dto.RegistrationRequestDto;
import ua.aleksenko.courseservice.model.entity.Role;
import ua.aleksenko.courseservice.model.entity.User;
import ua.aleksenko.courseservice.model.exception.UserAlreadyRegisteredException;
import ua.aleksenko.courseservice.repository.UserRepository;
import ua.aleksenko.courseservice.service.JwtService;
import ua.aleksenko.courseservice.service.RegistrationService;

@Service
@Slf4j
@RequiredArgsConstructor
public class RegistrationServiceImpl implements RegistrationService {

	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;
	private final JwtService jwtService;

	@Override
	@Transactional
	public AuthenticationResponseDto registerUser(RegistrationRequestDto dto) {
		log.info("Start registering user: {}", dto.getEmail());
		userRepository.findByEmailIgnoreCase(dto.getEmail())
				.ifPresent(throwUserAlreadyRegisteredException());
		User user = userRepository.save(
				new User(dto.getFirstName(),
						dto.getLastName(),
						dto.getEmail(),
						passwordEncoder.encode(dto.getPassword()),
						Role.USER)
		);
		log.info("Finish registering user: {}", dto.getEmail());
		return new AuthenticationResponseDto(jwtService.generateToken(user));
	}

	public Consumer<User> throwUserAlreadyRegisteredException() {
		return user -> {
			throw new UserAlreadyRegisteredException(user.getEmail());
		};
	}

}
