package ua.aleksenko.courseservice.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;

import lombok.extern.slf4j.Slf4j;
import ua.aleksenko.courseservice.model.entity.User;
import ua.aleksenko.courseservice.service.JwtService;

@Service
@Slf4j
public class JwtServiceImpl implements JwtService {

	@Value(value = "${jwt.secret}")
	private String secretKey;

	@Value(value = "${jwt.validity}")
	private long validityInMilli;

	@Value(value = "${jwt.issuer}")
	private String issuer;

	@Override
	public String generateToken(User user) {
		return JWT.create()
				.withSubject(user.getEmail())
				.withIssuer(issuer)
				.withClaim("role", user.getRole().toString())
				.withExpiresAt(new Date(System.currentTimeMillis() + validityInMilli))
				.withIssuedAt(new Date(System.currentTimeMillis()))
				.sign(Algorithm.HMAC256(secretKey));
	}

	@Override
	public void validateToken(String token) {
		Algorithm algorithm = Algorithm.HMAC256(secretKey);
		JWTVerifier verifier = JWT.require(algorithm)
				.withIssuer(issuer)
				.withClaimPresence("role")
				.build();

		verifier.verify(token);
	}
}
