package ua.aleksenko.courseservice.service;

import ua.aleksenko.courseservice.model.dto.AuthenticationResponseDto;
import ua.aleksenko.courseservice.model.dto.LoginRequestDto;

public interface LoginService {

	AuthenticationResponseDto login(LoginRequestDto dto);
}
