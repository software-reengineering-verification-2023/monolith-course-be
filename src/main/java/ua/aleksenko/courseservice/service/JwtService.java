package ua.aleksenko.courseservice.service;


import ua.aleksenko.courseservice.model.entity.User;

public interface JwtService {

	String generateToken(User user);

	void validateToken(String token);
}
