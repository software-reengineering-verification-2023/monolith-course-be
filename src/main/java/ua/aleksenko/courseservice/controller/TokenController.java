package ua.aleksenko.courseservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ua.aleksenko.courseservice.service.JwtService;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/token")
public class TokenController {

	private final JwtService jwtService;

	@SecurityRequirement(name = "")
	@GetMapping("/validate")
	public void validateToken(@RequestParam String token) {
		log.info("Try to validate token: {}", token);
		jwtService.validateToken(token);
	}
}
