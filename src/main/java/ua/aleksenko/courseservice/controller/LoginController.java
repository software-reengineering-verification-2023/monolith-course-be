package ua.aleksenko.courseservice.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import ua.aleksenko.courseservice.model.dto.AuthenticationResponseDto;
import ua.aleksenko.courseservice.model.dto.LoginRequestDto;
import ua.aleksenko.courseservice.service.LoginService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/login")
@Validated
public class LoginController {

	private final LoginService loginService;

	@SecurityRequirement(name = "")
	@PostMapping
	public AuthenticationResponseDto login(@Valid @RequestBody LoginRequestDto dto) {
		return loginService.login(dto);
	}
}
