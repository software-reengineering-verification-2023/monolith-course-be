package ua.aleksenko.courseservice.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import ua.aleksenko.courseservice.model.dto.CourseDetailsDto;
import ua.aleksenko.courseservice.model.dto.CourseItemDto;
import ua.aleksenko.courseservice.model.dto.CreateCourseDto;
import ua.aleksenko.courseservice.model.dto.PageResponse;
import ua.aleksenko.courseservice.model.dto.UpdateCourseDto;
import ua.aleksenko.courseservice.model.specification.SearchRequest;
import ua.aleksenko.courseservice.service.CourseService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/course")
public class CourseController {

	private final CourseService courseService;

	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN')")
	@PostMapping("/search")
	public PageResponse<CourseItemDto> searchCourses(@RequestBody SearchRequest request) {
		return courseService.searchCourses(request);
	}

	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN')")
	@GetMapping("/{id}")
	public CourseDetailsDto getCourseDetails(@PathVariable UUID id) {
		return courseService.getCourseDetails(id);
	}

	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN')")
	@GetMapping("/categories")
	public List<String> getAllCategories() {
		return courseService.getAllCategories();
	}

	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN')")
	@GetMapping("/languages")
	public List<String> getAllLanguages() {
		return courseService.getAllLanguages();
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@DeleteMapping("/delete/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteCourse(@PathVariable UUID id) {
		courseService.deleteCourse(id);
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@PostMapping("/create")
	@ResponseStatus(HttpStatus.CREATED)
	public String createCourse(@RequestBody CreateCourseDto dto) {
		return courseService.create(dto).toString();
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@PutMapping("/update")
	@ResponseStatus(HttpStatus.OK)
	public void updateCourse(@RequestBody UpdateCourseDto dto) {
		courseService.update(dto);
	}
}
