package ua.aleksenko.courseservice.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import ua.aleksenko.courseservice.model.dto.AuthenticationResponseDto;
import ua.aleksenko.courseservice.model.dto.RegistrationRequestDto;
import ua.aleksenko.courseservice.service.RegistrationService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/registration")
public class RegistrationController {

	private final RegistrationService registrationService;

	@SecurityRequirement(name = "")
	@PostMapping
	public AuthenticationResponseDto registerUser(@Valid @RequestBody RegistrationRequestDto dto) {
		return registrationService.registerUser(dto);
	}
}
