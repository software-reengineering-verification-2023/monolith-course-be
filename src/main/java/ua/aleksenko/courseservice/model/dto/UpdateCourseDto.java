package ua.aleksenko.courseservice.model.dto;

import java.util.List;
import java.util.UUID;

public record UpdateCourseDto(
		UUID id,
		String title,
		String description,
		String source,
		String category,
		String language,
		String imageUrl,
		List<UUID> lessonsToDelete,
		List<LessonDto> editedLessons,
		List<String> newLessons) {
}
