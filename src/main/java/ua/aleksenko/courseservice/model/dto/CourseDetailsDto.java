package ua.aleksenko.courseservice.model.dto;

import java.time.LocalDate;
import java.util.List;

public record CourseDetailsDto(
		String title,
		String description,
		String imageUrl,
		String language,
		String category,
		String source,
		LocalDate createdAt,
		LocalDate updatedAt,
		List<LessonDto> lessons) {
}
